import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.*;

public class AutobuyticketsGUI{
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextArea textArea1;
    private JLabel Label2;
    private JButton button7;
    private  JLabel Totalcash;
    private JPanel root;
    private JLabel Label1;
    private JButton cancelButton;


    public int totalcost=0;
    private String initialtext = textArea1.getText();
    public static void main(String[] args) {
        JFrame frame = new JFrame("AutobuyticketsGUI");
        frame.setContentPane(new AutobuyticketsGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }

    public void Displaytotalcost(String Food,int cost){

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+Food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            totalcost+=cost;
            textArea1.append(Food + "　：　" + cost + " yen\n");
            Totalcash.setText("Total cost : "+totalcost+" yen");
            JOptionPane.showMessageDialog(null,"Thank you for ordering "+Food+"! ");
        }
    }
    public AutobuyticketsGUI() {

    button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Displaytotalcost("Tsukemen",900);
        }
    });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Displaytotalcost("PigNiboshi",860);
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Displaytotalcost("KidsRamen",560);
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Displaytotalcost("ShouyuRamen",800);
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Displaytotalcost("PigRice",260);
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Displaytotalcost("Rice",160);
            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JSample3_1 frame1 = new JSample3_1("Add toppings",totalcost);
                   frame1.setVisible(true);

                }


        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea1.setText(initialtext);
                totalcost=0;
                Totalcash.setText("Total cost : "+totalcost+" yen");
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
class JSample3_1 extends JFrame{
    private int Allcost=0;
    JSample3_1(String title,int cost){
        Allcost = cost;
        AutobuyticketsGUI AG = new AutobuyticketsGUI();
        setTitle(title);
        setBounds(100, 100, 600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();

        JButton button1 = new JButton("Add Gyouza");
        JButton button2 = new JButton("Extra Beef");
        JButton button3 = new JButton("Next");
        JLabel  total_cost = new JLabel("totalcost:"+Allcost+"yen");
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(total_cost);
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Allcost+=150;
                total_cost.setText("Total cost : "+Allcost+" yen");
            }

        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Allcost+=200;
                total_cost.setText("Total cost : "+Allcost+" yen");
            }

        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Is the customer's payment amount " + Allcost + " yen?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you!\n Please wait for a while until the product arrives.");
                    Allcost =0;
                    setVisible(false);

                }}

        });
        Container contentPane = getContentPane();
        contentPane.add(panel, BorderLayout.CENTER);

    }
}
